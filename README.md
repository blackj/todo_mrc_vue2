# TodoMVC

<img src="./TodoMVC.png" width="400">

## Client setup

```sh
cd client-vue-3 # cd client-vue-2

yarn install
yarn serve
```

## Server

### Setup

```
cd server

yarn install
yarn serve
```

### Usage

```
GET    http://localhost:3000/todos
GET    http://localhost:3000/todos/1
POST   http://localhost:3000/todos
PUT    http://localhost:3000/todos/1
PATCH  http://localhost:3000/todos/1
DELETE http://localhost:3000/todos/1
```

# 功能需求
1. 可以新增待辦事項
2. 可以編輯待辦事項
3. 可以完成待辦事項
4. 可以復原完成的待辦事項
5. 可以刪除待辦事項
6. 可以刪除已完成的待辦事項
7. 可以用「全部、已完成、未完成」篩選待辦事項
8. 重整頁面後，保持舊有所有狀態（待辦事項、篩選狀態）
9. 接入CRUD API
